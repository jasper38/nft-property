# NFT屬性格式
[NFT屬性.md](NFT屬性.md)
- EIP-721 format
	- test url: https://frozen-retreat-02069.herokuapp.com/demo/pro/test

- EIP-1155 format
	- test url https://frozen-retreat-02069.herokuapp.com/demo/pro/test2

- References
	1. opensea standard: https://docs.opensea.io/docs/metadata-standards
	2. eip-1155 standard: https://github.com/ethereum/EIPs/blob/master/EIPS/eip-1155.md#erc-1155-metadata-uri-json-schema