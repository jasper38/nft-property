package com.example.demo.dto;

import lombok.Data;

import java.util.List;

@Data
public class PastEventResDto {
    private String timestamp;
    private String blockHash;
    private String gasUsed;
    private String address;
    private String logIndex;
    private String data;
    private List<String> topics;
    private String blockNumber;
    private String transactionIndex;
    private String transactionHash;
    private String gasPrice;
}
