package com.example.demo.dto;

import lombok.Data;

@Data
public class PastEventReqDto {
    private String contractAddress;
    private Long startBlock;
    private Long endBlock;
    private String topic0;
    private String topic1;
    private String topic2;
}
