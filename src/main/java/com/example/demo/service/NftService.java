package com.example.demo.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.dto.PastEventReqDto;
import com.example.demo.dto.PastEventResDto;
import com.example.demo.dto.TransferEventTopicDto;
import com.example.demo.exception.SystemRuntimeException;
import com.example.demo.exception.enumeration.SystemExceptionEnum;
import com.example.demo.util.HttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.web3j.abi.TypeEncoder;
import org.web3j.abi.datatypes.Address;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class NftService {
    @Autowired
    private HttpUtil httpUtil;
    @Value("${contract.address}")
    private String contractAddress;

    @Value("${contract.creation}")
    private Long contractCreationBlock;

    @Value("${scan.url}")
    private String apiUrl;

    @Value("${scan.apiKey}")
    private String apiKey;


    public List<PastEventResDto> getTransfers(String filteredAddress,String type) {
        PastEventReqDto reqDto = new PastEventReqDto();
        reqDto.setContractAddress(contractAddress);
        reqDto.setStartBlock(contractCreationBlock);
        reqDto.setTopic0("0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef");
        if(type.equals("FROM")) {
            reqDto.setTopic1(filteredAddress);
        }
        else if(type.equals("TO")){
            reqDto.setTopic2(filteredAddress);
        }
        String mumbaiApi = apiUrl;
        String module = "logs";
        String action = "getLogs";
        String fromBlock = reqDto.getStartBlock().toString();
        String toBlock = reqDto.getEndBlock() == null ? "latest" : reqDto.getEndBlock().toString();
        String address = reqDto.getContractAddress();
        String topic0 = reqDto.getTopic0();
        String topic1 = "0x" + TypeEncoder.encode(new Address(reqDto.getTopic1() == null ? "0x0000000000000000000000000000000000000000" : reqDto.getTopic1()));
        //String topic1_2_opr = "or";
        String topic2 = "0x" + TypeEncoder.encode(new Address(reqDto.getTopic2() == null ? "0x0000000000000000000000000000000000000000" : reqDto.getTopic2()));
        String mumbaiApiKey = apiKey;

        String reqUrl = mumbaiApi
                .concat("module=").concat(module)
                .concat("&action=").concat(action)
                .concat("&fromBlock=").concat(fromBlock)
                .concat("&toBlock=").concat(toBlock)
                .concat("&address=").concat(address)
                .concat("&topic0=").concat(topic0)
                .concat(type.equals("FROM") ? "&topic1=" : "&topic2=").concat(type.equals("FROM") ? topic1 : topic2)
                .concat("&apikey=").concat(mumbaiApiKey);
        //log.debug("reqUrl: {}",reqUrl);
        String result = httpUtil.get(reqUrl, 2);
        JSONObject obj = JSON.parseObject(result);
        //log.debug("json status: {}", obj.get("status"));
        // 1: Ok, 0: not ok
        if(obj.get("status").toString().equals("1")){
            String resultString = obj.getString("result");
            List<PastEventResDto> resList = JSON.parseArray(resultString,PastEventResDto.class);
            //log.debug("resList: {}", resList);
            // parse event return value from hex to specific type
            List<String> topics = resList.get(0).getTopics();
            TransferEventTopicDto dto = parseTransferEvent(topics);
            //log.debug("Topics: {}",dto);
            return resList;
        }
        else {
            throw new SystemRuntimeException(SystemExceptionEnum.DATA_ERROR);
        }
    }

    public TransferEventTopicDto parseTransferEvent(List<String> topics){
        if(topics.size() != 4){
            // event value size error
            return null;
        }
        else{
            // from address topic 1
            String fromAddress = (String) parseTopicValue(topics.get(1),"ADDRESS");
            //log.debug("fromAddress: {}",fromAddress);
            // from address topic 1
            String toAddress = (String) parseTopicValue(topics.get(2),"ADDRESS");
            //log.debug("toAddress: {}",toAddress);
            // token id topic3
            BigInteger tokenId = (BigInteger) parseTopicValue(topics.get(3),"UINT");
            //log.debug("tokenId: {}",tokenId);
            return TransferEventTopicDto.builder()
                    .from(fromAddress)
                    .to(toAddress)
                    .tokenId(tokenId)
                    .build();
        }
    }

    public Object parseTopicValue(String topic, String type){
        switch (type){
            case "ADDRESS":
                Address address = new Address(topic);
                return address.toString();
            case "UINT":
                // Remove the prefix 0x
                if(topic.startsWith("0x")) {
                    topic = topic.substring(2);
                }
                return new BigInteger(topic, 16);
            default:
                return null;
        }
    }

    public List<BigInteger> getNftListByOwner(String address){
        List<PastEventResDto> pastEvents = getTransfers(address,"FROM");
        pastEvents.addAll(getTransfers(address,"TO"));
        List<TransferEventTopicDto> parsedEvents = new ArrayList<>();
        pastEvents.forEach(
                event -> {
                    parsedEvents.add(parseTransferEvent(event.getTopics()));
                }
        );
        Map<BigInteger,Integer> tokenMap = new HashMap<>();
        parsedEvents.forEach(
                parsedEvent -> {
                    if(!tokenMap.containsKey(parsedEvent.getTokenId())){
                        tokenMap.put(parsedEvent.getTokenId(),0);
                    }
                    //log.debug("parsedEvent: {}",parsedEvent);
                    if(parsedEvent.getFrom().equals(address)){
                        tokenMap.computeIfPresent(parsedEvent.getTokenId(), (k,v) -> v - 1);
                        //log.debug("From {}: {}",parsedEvent.getTokenId(),tokenMap.get(parsedEvent.getTokenId()));
                    }
                    if(parsedEvent.getTo().equals(address)){
                        tokenMap.computeIfPresent(parsedEvent.getTokenId(), (k,v) -> v + 1);
                        //log.debug("To {}: {}",parsedEvent.getTokenId(),tokenMap.get(parsedEvent.getTokenId()));
                    }
                }
        );

        List<BigInteger> result = tokenMap.entrySet().stream().filter(map -> map.getValue() > 0).map(Map.Entry::getKey).collect(Collectors.toList());
        log.debug("result: {}",result);
        return result;
    }

}
