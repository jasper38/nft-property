package com.example.demo.controller;

import com.example.demo.response.ResponseModel;
import com.example.demo.service.NftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/nft")
public class NftController {
    @Autowired
    private NftService nftService;


    @GetMapping("/transfer")
    public ResponseModel getNftTransfers(String address,String type){
        return ResponseModel.success(nftService.getTransfers(address,type));
    }

    @GetMapping("/list")
    public ResponseModel getNftListByOwner(String address){
        return ResponseModel.success(nftService.getNftListByOwner(address));
    }

}
