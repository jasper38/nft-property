package com.example.demo.util;

import com.example.demo.exception.SystemRuntimeException;
import com.example.demo.exception.enumeration.SystemExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class HttpUtil {

    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    public String post(String url,String json) {

        OkHttpClient client = new OkHttpClient();

        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        try {
            Response response = client.newCall(request).execute();
            // if (response.isSuccessful()) {
            return response.body().string();
            // }
        } catch (IOException e) {
            log.error("[http][post][{}]", e.getMessage());
            throw new SystemRuntimeException(SystemExceptionEnum.HTTP_REQUEST_ERROR);
        }
        // return null;
    }

    public String get(String url) {
        return get(url, 10); // Time out預設是10
    }

    public String get(String url, int timeoutSec) {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(timeoutSec, TimeUnit.SECONDS)
                .readTimeout(timeoutSec, TimeUnit.SECONDS).build();
        Request request = new Request.Builder()
                .url(url)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                return response.body().string();
            }
        } catch (SocketTimeoutException e) {
            log.error("[http][get][{}]", e.getMessage());
            throw new SystemRuntimeException(SystemExceptionEnum.REQUEST_TIMEOUT);
        } catch (IOException e) {
            log.error("[http][get][{}]", e.getMessage());
            throw new SystemRuntimeException(SystemExceptionEnum.HTTP_REQUEST_ERROR);
        }
        return null;
    }
}

