package com.example.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("pro")
public class PropertyController {
    @Autowired
    private PropertyService propertyService;

    public JSONObject getProperty(Long id){
        return propertyService.PropertyFactory(id);
    }

    @GetMapping("/test")
    public JSONObject getFTestProperty(){
        return propertyService.PropertyFactoryTestF(0L);
    }

    @GetMapping("/test2")
    public JSONObject getGKTestProperty(){
        return propertyService.PropertyFactoryTestGK(0L);
    }
}
