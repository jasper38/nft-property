package com.example.demo.service;

import com.example.demo.DemoApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = DemoApplication.class)
class NftServiceTest {
    @Autowired
    NftService nftService;
    @Test
    void getTransfers() {
        nftService.getTransfers("0x764b1b6b0562406a06b4923e99e25418360f54e6","FROM");
    }

    @Test
    void getNftListByOwner() {
        nftService.getNftListByOwner("0x764b1b6b0562406a06b4923e99e25418360f54e6");
    }
}