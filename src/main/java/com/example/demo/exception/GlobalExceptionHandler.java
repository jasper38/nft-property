package com.example.demo.exception;

import com.example.demo.exception.enumeration.SystemExceptionEnum;
import com.example.demo.response.ResponseEnum;
import com.example.demo.response.ResponseModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;



@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

	// @off
	/**
	 * 自定義錯誤 捕捉
	 * @param systemRuntimeException
	 * @return
	 */
	@ExceptionHandler(SystemRuntimeException.class)
	public ResponseModel handleSystemRuntimeException(SystemRuntimeException systemRuntimeException) {
		log.error("發生自定義錯誤－code：[{}]，message：[{}],class1:[{}],line:[{}],class2:[{}],line:[{}]",
				systemRuntimeException.getCode(),
				systemRuntimeException.getMessage(),
				systemRuntimeException.getStackTrace()[0].getClassName().substring(systemRuntimeException.getStackTrace()[0].getClassName().lastIndexOf(".") + 1),
				systemRuntimeException.getStackTrace()[0].getLineNumber(),
				systemRuntimeException.getStackTrace()[1].getClassName().substring(systemRuntimeException.getStackTrace()[1].getClassName().lastIndexOf(".") + 1),
				systemRuntimeException.getStackTrace()[1].getLineNumber());
		return ResponseModel.builder()
				.code(systemRuntimeException.getCode())
				.message(systemRuntimeException.getMessage())
				.build();
	}

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseModel handleMaxUploadSizeExceededException(MaxUploadSizeExceededException e) {
		log.error("上傳檔案：超過最大上傳大小");
		e.printStackTrace();
		return ResponseModel.builder()
				.code(SystemExceptionEnum.MAX_UPLOAD_SIZE_EXCEEDED.getCode())
				.message(SystemExceptionEnum.MAX_UPLOAD_SIZE_EXCEEDED.getMessage())
				.build();
    }

	/**
	 * RequestParam 參數沒送的錯誤 捕捉
	 * @param e
	 * @return
	 */
	@ExceptionHandler({ MissingServletRequestParameterException.class})
	public ResponseModel handleMissingServletRequestParameterException(MissingServletRequestParameterException e) {
		log.error("發生請求參數錯誤[RequestParam]:[{}]", e.getMessage());
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(e.getParameterName()).append("：").append("未傳此參數").append("]");
		return ResponseModel.builder()
				.code(ResponseEnum.VALID_ERROR.getCode())
				.message(ResponseEnum.VALID_ERROR.getMessage())
				.data(sb)
				.build();
	}


	/**
	 * Valid & Validated 檢核沒過 捕捉
	 *
	 * @param e
	 * @return
	 */
	@ExceptionHandler({MethodArgumentNotValidException.class})
	public ResponseModel handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
		log.error("發生請求參數錯誤[NotBlank]:[{}]", e.getMessage());

		StringBuilder sb = new StringBuilder();
		e.getBindingResult().getFieldErrors().forEach(fieldError -> {
			sb.append("[");
			sb.append(fieldError.getField()).append("：").append(fieldError.getDefaultMessage()).append("]");
		});
		return ResponseModel.builder()
				.code(ResponseEnum.VALID_ERROR.getCode())
				.message(ResponseEnum.VALID_ERROR.getMessage())
				.data(sb.toString())
				.build();
	}
	@ExceptionHandler({BindException.class})
	public ResponseModel handleBindException(BindException e) {
		log.error("發生請求參數錯誤[NotBlank]:[{}]", e.getMessage());
		StringBuilder sb = new StringBuilder();
		e.getBindingResult().getFieldErrors().forEach(fieldError -> {
			sb.append("[");
			sb.append(fieldError.getField()).append("：").append(fieldError.getDefaultMessage()).append("]");
		});
		return ResponseModel.builder()
				.code(ResponseEnum.VALID_ERROR.getCode())
				.message(ResponseEnum.VALID_ERROR.getMessage())
				.data(sb.toString())
				.build();
	}

	/**
	 * 請求方法錯誤 捕捉
	 * @param e
	 * @return
	 */
	@ExceptionHandler({HttpRequestMethodNotSupportedException.class})
	public ResponseModel handleMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
		log.error("發生請求方法錯誤[RequestMethod]:[{}]", e.getMessage());
		return ResponseModel.builder()
				.code(ResponseEnum.VALID_ERROR.getCode())
				.message(ResponseEnum.VALID_ERROR.getMessage())
				.data(e.getMessage())
				.build();
	}

	/**
	 * 未知錯誤捕捉
	 * @param exception
	 * @return
	 */
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	public ResponseModel handleException(Exception exception) {
		log.error("程式發生未知錯誤，請修正！！！");
		exception.printStackTrace();
		return ResponseModel.builder()
				.code(ResponseEnum.SYSTEM_ERROR.getCode())
				.message(ResponseEnum.SYSTEM_ERROR.getMessage())
				.build();
	}

	// @on

}
