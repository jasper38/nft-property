package com.example.demo.exception.enumeration;

import com.example.demo.exception.IRuntimeExceptionEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum SystemExceptionEnum implements IRuntimeExceptionEnum {

	//@off
	PARAM_ERROR("ES_0001", "PARAM_ERROR", "參數錯誤"),
	INSERT_ERROR("ES_0002", "INSERT_ERROR", "新增失敗"),
	UPDATE_ERROR("ES_0003", "UPDATE_ERROR", "修改失敗"),
	DATA_ERROR("ES_0004", "DATA_ERROR", "資料錯誤"),
	UPLOAD_ERROR("ES_0005", "UPLOAD_ERROR", "上傳檔案失敗"),
	UPLOAD_FILE_NOT_SUPPORTED("ES_0006", "UPLOAD_FILE_NOT_SUPPORTED", "上傳檔案不支援此檔案類型"),
	NO_DATA("ES_0007", "NO_DATA", "查無資料"),
	MAX_UPLOAD_SIZE_EXCEEDED("ES_0008", "MAX_UPLOAD_SIZE_EXCEEDED", "超過最大上傳大小"),
	HTTP_REQUEST_ERROR("ES_0009", "HTTP_REQUEST_ERROR", "請求錯誤"),
	SMS_TEMPLATE_CODE_ERROR("ES_0010", "SMS_TEMPLATE_CODE_ERROR", "簡訊文本新增錯誤"),
	SMS_TEMPLATE_CODE_DUPLICATED_ERROR("ES_0011", "SMS_TEMPLATE_CODE_DUPLICATED_ERROR", "簡訊文本重複新增"),
	SMS_TEMPLATE_MATCH_ERROR("ES_0012", "SMS_TEMPLATE_MATCH_ERROR", "查無匹配簡訊模板"),
	SMS_ORDER_NOT_MATCH_ERROR("ES_0013", "SMS_ORDER_NOT_MATCH_ERROR", "查無匹配訂單"),
	SMS_ORDER_DUPLICATED_MATCH_ERROR("ES_0014", "SMS_ORDER_DUPLICATED_MATCH_ERROR", "匹配多筆訂單"),
	SMS_USER_MACHINE_NOT_FOUND_ERROR("ES_0015", "USER_MACHINE_NOT_FOUND_ERROR", "查無機器"),
	SMS_CONTENT_DUPLICATED_ERROR("ES_0016", "SMS_CONTENT_DUPLICATED_ERROR", "簡訊重複發送"),
	SMS_ORDER_NOT_FOUND_ERROR("ES_0017", "SMS_ORDER_NOT_FOUND_ERROR", "查無簡訊"),
	SMS_MATCH_TYPE_ERROR("ES_0018", "SMS_MATCH_TYPE_ERROR", "查無訂單匹配狀態"),
	REQUEST_TIMEOUT("ES_0019", "REQUEST_TIMEOUT", "請求超時"),
	JDBC_CLASS_CAST_ERROR("ES_0020", "JDBC_CLASS_CAST_ERROR", "資料庫型別轉換Dto失敗"),

	;

	private String code;
	private String message;
	private String memo;

	//@on
}
