package com.example.demo.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.constant.PropertyEnum;
import com.example.demo.dto.PropertyDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PropertyService {
    @Autowired
    private RandomService randomService;
    public JSONObject PropertyFactory(Long id){
        JSONObject res = new JSONObject();
        JSONArray parents = new JSONArray();
        JSONArray children = new JSONArray();
        res.put("TokenId",id);
        res.put("parents",parents);
        res.put("children",children);
        return res;
    }
    //EIP-1155
    public JSONObject PropertyFactoryTestF(Long id){
        JSONObject attributes = new JSONObject();
        JSONObject res = new JSONObject();
        JSONObject array = new JSONObject();
        PropertyDTO dto = randomService.FTest();
        JSONArray parents = new JSONArray();
        JSONArray children = new JSONArray();
        res.put("name",dto.getName());
        res.put("description","This is a test Forward");
        res.put("image","https://picsum.photos/200/300.jpg");
        //Int
        buildProperty(attributes,"level",dto.getLevel(),false);
        buildProperty(attributes,"vit",dto.getVit(),false);
        buildProperty(attributes,"atk",dto.getAtk(),false);
        buildProperty(attributes,"luk",dto.getLuk(),false);
        buildProperty(attributes,"agi",dto.getAgi(),false);
        //String
        buildProperty(attributes,"rarity",dto.getRarity(),false);
        buildProperty(attributes,"position",dto.getPosition(),false);
        buildProperty(attributes,"race",dto.getRace(),false);
        buildProperty(attributes,"eyes",dto.getEyes(),false);
        buildProperty(attributes,"nosemouth",dto.getNoseAndMouse(),false);
        buildProperty(attributes,"ears",dto.getEars(),false);
        buildProperty(attributes,"hair",dto.getBack(),false);
        buildProperty(attributes,"tail",dto.getTail(),false);
        buildProperty(attributes,"tattoos",dto.getTattoo(),false);
        //Array
        buildProperty(attributes,"skills",dto.getSkills(),true);
        buildProperty(attributes,"parents",parents,true);
        buildProperty(attributes,"children",children,true);
        res.put("properties",attributes);
        return res;
    }
    public void buildProperty(JSONObject object, String name, Object value, boolean is_array){
        JSONObject newObject = new JSONObject();
        newObject.put("name",name);
        newObject.put("value",value);
        newObject.put("display_value",value);
        newObject.put("class","emphasis");
        if(!is_array) {
            newObject.put("css", "");
        }
        object.put(name,newObject);
    }
    //EIP-721
    public JSONObject PropertyFactoryTestGK(Long id) {
        JSONArray attributes = new JSONArray();
        JSONObject res = new JSONObject();
        PropertyDTO dto = randomService.CTest();
        res.put("name",dto.getName());
        res.put("description","This is a test Center");
        res.put("image","https://picsum.photos/200/300.jpg");
        //Int
        JSONBuilder721(attributes,"level",dto.getLevel(),PropertyEnum.rank);
        JSONBuilder721(attributes,"vit",dto.getVit(),PropertyEnum.number);
        JSONBuilder721(attributes,"atk",dto.getAtk(),PropertyEnum.number);
        JSONBuilder721(attributes,"luk",dto.getLuk(),PropertyEnum.number);
        JSONBuilder721(attributes,"agi",dto.getAgi(),PropertyEnum.number);
        //String
        JSONBuilder721(attributes,"rarity",dto.getRarity(),PropertyEnum.string);
        JSONBuilder721(attributes,"position",dto.getPosition(),PropertyEnum.string);
        JSONBuilder721(attributes,"race",dto.getRace(),PropertyEnum.string);
        JSONBuilder721(attributes,"eyes",dto.getEyes(),PropertyEnum.string);
        JSONBuilder721(attributes,"nosemouth",dto.getNoseAndMouse(),PropertyEnum.string);
        JSONBuilder721(attributes,"ears",dto.getEars(),PropertyEnum.string);
        JSONBuilder721(attributes,"hair",dto.getBack(),PropertyEnum.string);
        JSONBuilder721(attributes,"tail",dto.getTail(),PropertyEnum.string);
        JSONBuilder721(attributes,"tattoos",dto.getTattoo(),PropertyEnum.string);
        res.put("attributes",attributes);
        return res;
    }
    public void JSONBuilder721(JSONArray array, String name, Object value, PropertyEnum type) {
        JSONObject newObject = new JSONObject();
        if(type != PropertyEnum.string) {
            if(type != PropertyEnum.rank) {
                newObject.put("display_type", type.getTitle());
            }
        }
        newObject.put("trait_type",name);
        newObject.put("value",value);
        array.add(newObject);
    }
}
