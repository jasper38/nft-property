package com.example.demo.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public enum PropertyEnum {
    number("number"),
    date("date"),
    boost("boost_number"),
    percentage("boost_percentage"),
    string("normal"),
    rank("rank");

    @Setter
    private String title;
}
