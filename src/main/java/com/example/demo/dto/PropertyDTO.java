package com.example.demo.dto;

import lombok.Data;


@Data
public class PropertyDTO {
    private String name;
    private int level;
    private String rarity;
    private int vit;
    private int atk;
    private int agi;
    private int luk;
    private String eyes;
    private String ears;
    private String noseAndMouse;
    private String hair;
    private String tail;
    private String back;
    private String tattoo;
    private String race;
    private String position;
    private String [] skills;
}
