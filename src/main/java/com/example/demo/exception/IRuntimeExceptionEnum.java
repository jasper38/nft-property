package com.example.demo.exception;

public interface IRuntimeExceptionEnum {

	public String getCode();

	public String getMessage();

}
