package com.example.demo.service;

import com.example.demo.dto.PropertyDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RandomService {
    public PropertyDTO random(){
        PropertyDTO random = new PropertyDTO();
        return random;
    }
    public PropertyDTO FTest(){
        PropertyDTO random = new PropertyDTO();
        random.setName("Monster Bruno");
        random.setLevel(60);
        random.setRarity("A");
        random.setPosition("F");
        random.setRace("水");
        random.setVit(746);
        random.setAtk(326);
        random.setLuk(329);
        random.setAgi(329);
        String [] skills = {"skill_00_100","skill_00_200","skill_00_300"};
        random.setSkills(skills);
        random.setEyes("eye_01_01");
        random.setNoseAndMouse("nosemouth_01_01");
        random.setEars("ear_01_01");
        random.setHair("hair_01_01");
        random.setBack("back_01_01");
        random.setTail("tail_01_01");
        random.setTattoo("tattoos_01_01");
        return random;
    }
    public PropertyDTO CTest(){
        PropertyDTO random = new PropertyDTO();
        random.setName("Monster Tom");
        random.setLevel(60);
        random.setRarity("A");
        random.setPosition("C");
        random.setRace("木");
        random.setVit(418);
        random.setAtk(578);
        random.setLuk(368);
        random.setAgi(368);
        String [] skills = {"skill_00_100","skill_00_200","skill_00_300"};
        random.setSkills(skills);
        random.setEyes("eye_01_02");
        random.setNoseAndMouse("nosemouth_01_02");
        random.setEars("ear_01_02");
        random.setHair("hair_01_02");
        random.setBack("back_01_02");
        random.setTail("tail_01_02");
        random.setTattoo("tattoos_01_02");
        return random;
    }
}
