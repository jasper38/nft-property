## NFT設定
### NFT屬性 opensea standard
- EIP-721格式
- example: https://testnets.opensea.io/assets/0x053d1f655dead2ed80341bae9aafc260606b7818/2
```json
{
    "image": "https://picsum.photos/200/300.jpg",
    "name": "Monster Tom",
    "description": "This is a test Center",
    "attributes": [
        {
            "value": 60,
            "trait_type": "level"
        },
        {
            "display_type": "number",
            "value": 418,
            "trait_type": "vit"
        },
        {
            "display_type": "number",
            "value": 578,
            "trait_type": "atk"
        },
        {
            "display_type": "number",
            "value": 368,
            "trait_type": "luk"
        },
        {
            "display_type": "number",
            "value": 368,
            "trait_type": "agi"
        },
        {
            "value": "A",
            "trait_type": "rarity"
        },
        {
            "value": "C",
            "trait_type": "position"
        },
        {
            "value": "木",
            "trait_type": "race"
        },
        {
            "value": "eye_01_02",
            "trait_type": "eyes"
        },
        {
            "value": "nosemouth_01_02",
            "trait_type": "nosemouth"
        },
        {
            "value": "ear_01_02",
            "trait_type": "ears"
        },
        {
            "value": "back_01_02",
            "trait_type": "hair"
        },
        {
            "value": "tail_01_02",
            "trait_type": "tail"
        },
        {
            "value": "tattoos_01_02",
            "trait_type": "tattoos"
        }
    ]
}
```
- EIP-1155 格式
- 陣列屬性無法在opensea列出(TO-DO)
- example: https://testnets.opensea.io/assets/0x053d1f655dead2ed80341bae9aafc260606b7818/1
```json
{
    "image": "https://picsum.photos/200/300.jpg",
    "name": "Monster Bruno",
    "description": "This is a test Forward",
    "properties": {
        "vit": {
            "display_value": 746,
            "css": "",
            "name": "vit",
            "value": 746,
            "class": "emphasis"
        },
        "agi": {
            "display_value": 329,
            "css": "",
            "name": "agi",
            "value": 329,
            "class": "emphasis"
        },
        "luk": {
            "display_value": 329,
            "css": "",
            "name": "luk",
            "value": 329,
            "class": "emphasis"
        },
        "race": {
            "display_value": "水",
            "css": "",
            "name": "race",
            "value": "水",
            "class": "emphasis"
        },
        "level": {
            "display_value": 60,
            "css": "",
            "name": "level",
            "value": 60,
            "class": "emphasis"
        },
        "tail": {
            "display_value": "tail_01_01",
            "css": "",
            "name": "tail",
            "value": "tail_01_01",
            "class": "emphasis"
        },
        "eyes": {
            "display_value": "eye_01_01",
            "css": "",
            "name": "eyes",
            "value": "eye_01_01",
            "class": "emphasis"
        },
        "nosemouth": {
            "display_value": "nosemouth_01_01",
            "css": "",
            "name": "nosemouth",
            "value": "nosemouth_01_01",
            "class": "emphasis"
        },
        "skills": {
            "display_value": [
                "skill_00_100",
                "skill_00_200",
                "skill_00_300"
            ],
            "name": "skills",
            "value": [
                "skill_00_100",
                "skill_00_200",
                "skill_00_300"
            ],
            "class": "emphasis"
        },
        "ears": {
            "display_value": "ear_01_01",
            "css": "",
            "name": "ears",
            "value": "ear_01_01",
            "class": "emphasis"
        },
        "hair": {
            "display_value": "back_01_01",
            "css": "",
            "name": "hair",
            "value": "back_01_01",
            "class": "emphasis"
        },
        "children": {
            "display_value": [],
            "name": "children",
            "value": [],
            "class": "emphasis"
        },
        "tattoos": {
            "display_value": "tattoos_01_01",
            "css": "",
            "name": "tattoos",
            "value": "tattoos_01_01",
            "class": "emphasis"
        },
        "atk": {
            "display_value": 326,
            "css": "",
            "name": "atk",
            "value": 326,
            "class": "emphasis"
        },
        "position": {
            "display_value": "F",
            "css": "",
            "name": "position",
            "value": "F",
            "class": "emphasis"
        },
        "rarity": {
            "display_value": "A",
            "css": "",
            "name": "rarity",
            "value": "A",
            "class": "emphasis"
        },
        "parents": {
            "display_value": [],
            "name": "parents",
            "value": [],
            "class": "emphasis"
        }
    }
}
```